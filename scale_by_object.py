#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##################################################################################
# Scale by object - Scale selected objects by changes to last selected object
# An Inkscape 1.2+ Extension
# Appears Under Extensions>Arrange>Scale By Object
##################################################################################

import inkex
from inkex import Transform

from inklinea import Inklin

import shutil, os
from copy import deepcopy
import uuid

class ScaleByObject(inkex.EffectExtension):

    def add_arguments(self, pars):

        # Main Notebook
        pars.add_argument("--scale_by_object_notebook", type=str, dest="scale_by_object_notebook", default=0)

        pars.add_argument("--scale_type_radio", type=str, dest="scale_type_radio", default='width')

        pars.add_argument("--bbox_type_radio", type=str, dest="bbox_type_radio", default='visual')

        pars.add_argument("--scale_proportion_cb", type=str, dest="scale_proportion_cb", default='true')

        pars.add_argument("--new_width_float", type=float, dest="new_width_float", default=20)
        pars.add_argument("--new_height_float", type=float, dest="new_height_float", default=20)

        pars.add_argument("--scale_units_combo", type=str, dest="scale_units_combo", default='px')


    def effect(self):

        selection_list = self.svg.selected
        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')
            return

        temp_folder = Inklin.make_temp_folder(self)

        if self.options.bbox_type_radio == 'geometric':
            temp_svg = deepcopy(self.svg)
            drawable_children = temp_svg.xpath(
                '//svg:circle | //svg:ellipse | //svg:line | //svg:path | //svg:text | //svg:polygon | //svg:polyline '
                '| //svg:rect | //svg:use')
            for item in drawable_children:
                item.style['fill'] = 'none'
                item.style['stroke'] = 'none'

            bbox_svg_text = temp_svg.tostring().decode('utf-8')

            temp_filename = str(uuid.uuid4()) + '.svg'
            temp_filepath = os.path.join(temp_folder, temp_filename)


            with open(temp_filepath, mode='w') as temp_svg_file:
                temp_svg_file.write(bbox_svg_text)

            bbox_svg_filepath = temp_filepath

        else:
            bbox_svg_filepath = self.options.input_file


        bbox_dict = Inklin.inkscape_command_call_bboxes_to_dict(self, bbox_svg_filepath)
        # x1 = TopLeft, x2 = BottomLeft, x3 = BottomRight, x4 = TopRight, mid_x and mid_y

        new_width = self.options.new_width_float
        new_height = self.options.new_height_float
        units = self.options.scale_units_combo

        unit_to_pixel_cf = Inklin.conversions[units]

        new_pixel_width = new_width * unit_to_pixel_cf
        new_pixel_height = new_height * unit_to_pixel_cf

        # Scale Object = 'so'
        so = selection_list[-1]
        so_id = so.get_id()
        so_pixel_width = bbox_dict[so_id]['width']
        so_pixel_height = bbox_dict[so_id]['height']

        scale_factor_x = new_pixel_width / so_pixel_width
        scale_factor_y = new_pixel_height / so_pixel_height

        if self.options.scale_type_radio == 'width':
            # scale_factor_x = new_pixel_width / so_pixel_width
            if self.options.scale_proportion_cb == 'true':
                scale_factor_y = scale_factor_x
            else:
                scale_factor_y = 1
        elif self.options.scale_type_radio == 'height':
            # scale_factor_y = new_pixel_height / so_pixel_height
            if self.options.scale_proportion_cb == 'true':
                scale_factor_x = scale_factor_y
            else:
                scale_factor_x = 1
        else:
            None

        scale_transform = Transform()
        scale_transform.add_scale(scale_factor_x, scale_factor_y)

        for item in selection_list:
            item.transform = scale_transform @ item.transform

        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

if __name__ == '__main__':
    ScaleByObject().run()
